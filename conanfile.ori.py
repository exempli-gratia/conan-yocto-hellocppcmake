from conans import ConanFile


class HelloCppCmakeConan(ConanFile):
     name = "hellocppcmake"
     version = "1.0.0"
     description = "HelloCppCmake"
     license = "MIT"
     settings = "os", "arch", "compiler", "build_type"
     generators = "cmake"
     #requires = "openssl/1.0.2u", "c-ares/1.15.0"

def source(self):
     #source_url = "https://github.com/eclipse/mosquitto"
     source_url = "https://gitlab.com/exempli-gratia/hellocppcmake"
     #source_url = "https://gitlab.com/exempli-gratia/hellocppcmake.git"
     tools.get("{0}/archive/v{1}.tar.gz".format(source_url, self.version))

def build(self):
     cmake = CMake(self)
     cmake.configure()
     cmake.build()

#def package(self):
#     self.copy("*.h", dst="include", src="hello")
#     self.copy("*.so", dst="lib", keep_path=False)
#     self.copy("*.a", dst="lib", keep_path=False)
#     self.copy("*mosquitto.conf", dst="bin", keep_path=False)

#def deploy(self):
#     # Deploy the executables from this eclipse/mosquitto package
#     self.copy("*", src="bin", dst="bin")
#     # Deploy the shared libs from this eclipse/mosquitto package
#     self.copy("*.so*", src="lib", dst="bin")
#     # Deploy all the shared libs from the transitive deps
#     self.copy_deps("*.so*", src="lib", dst="bin")

#def package_info(self):
#     self.cpp_info.libs = ["mosquitto", "mosquitopp", "rt", "pthread", "dl"]
