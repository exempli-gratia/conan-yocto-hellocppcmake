from conans import ConanFile, tools, CMake
from conans.errors import ConanInvalidConfiguration, ConanException
from conans.tools import Version



class HelloCppCmakeConan(ConanFile):
     name = "hellocppcmake"
     version = "1.0.0"
     description = "HelloCppCmake"
     license = "MIT"
     settings = "os", "arch", "compiler", "build_type"
     # cmake_paths supposedly does not make CMakeLists.txt dependant on conan
     generators = "cmake_paths"
     #requires = "openssl/1.0.2u", "c-ares/1.15.0"

     def source(self):
          #source_url = "https://github.com/eclipse/mosquitto"
          source_url = "https://gitlab.com/exempli-gratia/hellocppcmake"
          #source_url = "https://gitlab.com/exempli-gratia/hellocppcmake.git"
          #tools.get("{0}/archive/v{1}.tar.gz".format(source_url, self.version))
          git = tools.Git(folder=".")
          #git.verify_ssl="False"
          git.clone("https://gitlab.com/exempli-gratia/hellocppcmake.git", "master")
          #self.run("git clone https://gitlab.com/exempli-gratia/hellocppcmake.git")

     def build(self):
          cmake = CMake(self)
          cmake.configure()
          cmake.build()

     def package(self):
     #     self.copy(pattern, dst="", src="", keep_path=True, symlinks=None, excludes=None, ignore_case=True)
          self.copy("*.h", dst="include", src="hello")
          self.copy("*.so", dst="lib", keep_path=False)
          self.copy("*.a", dst="lib", keep_path=False)
          self.copy("hellocppcmake", dst="bin")
     #    self.copy("*mosquitto.conf", dst="bin", keep_path=False)

     #def deploy(self):
     #     # Deploy the executables from this eclipse/mosquitto package
     #     self.copy("*", src="bin", dst="bin")
     #     # Deploy the shared libs from this eclipse/mosquitto package
     #     self.copy("*.so*", src="lib", dst="bin")
     #     # Deploy all the shared libs from the transitive deps
     #     self.copy_deps("*.so*", src="lib", dst="bin")

     #def package_info(self):
     #     self.cpp_info.libs = ["mosquitto", "mosquitopp", "rt", "pthread", "dl"]


