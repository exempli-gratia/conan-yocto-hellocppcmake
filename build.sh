#!/bin/bash

source /opt/resy/3.3.66/multi-v7-ml/imx6q-phytec-mira-rdk-nand/environment-setup-armv7at2hf-neon-resy-linux-gnueabi

if [ ! -f ~/.conan/profiles/default ]; then
   mkdir ~/.conan/profiles
   cp default ~/.conan/profiles
   #mv ~/.conan/profiles/default ~/.conan/profiles/default.ori
fi

#conan create . user/channel --profile armv7hf

set -x
rm -rf top && mkdir top && cd top
#cp ../conanfile.py .
pwd
conan source .. --source-folder=tmp/source
conan install .. --install-folder=tmp/build
conan build .. --source-folder=tmp/source --build-folder=tmp/build
conan package .. --source-folder=tmp/source --build-folder=tmp/build --package-folder=tmp/package
conan info ..
##conan package -bf build .
## --> this seems to do stuff in ~/.conan/data
conan export-pkg --force --build-folder=tmp/build .. hellocppcmake/1.0.0@user/channel
## <-- this seems to do stuff in ~/.conan/data
## for this we need a test package:
## conan test test_package hellocppcmake/1.0.0@user/channel
#file tmp/build/hellocppcmake
##arm-resy-linux-gnueabi-readelf -a tmp/build/hellocppcmake
tree
tree ~/.conan
cd ..
set +x
